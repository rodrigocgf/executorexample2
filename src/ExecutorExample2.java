import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorExample2 {

	public static void main(String[] args) {
		System.out.println("ExecutorExample2");
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		List<Callable<Integer>> listOfCallable = Arrays.asList(
                () -> 1,
                () -> 2,
                () -> 3);
		
		try {

            List<Future<Integer>> futures = executor.invokeAll(listOfCallable);

            int sum = futures.stream().map(f -> {
                try {
                    return f.get();
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            }).mapToInt(Integer::intValue).sum();

            System.out.println(sum);

        } catch (InterruptedException e) {// thread was interrupted
            e.printStackTrace();
        } finally {

            // shut down the executor manually
            executor.shutdown();

        }

	}

}
